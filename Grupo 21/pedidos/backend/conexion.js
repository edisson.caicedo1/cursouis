const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/grupo21');
const miconexion = mongoose.connection;
miconexion.on('connected',()=>{
    console.log('Conexión a la base de datos');
});
miconexion.on('error',()=>{
    console.log('No conectado.');
});
module.exports=mongoose;