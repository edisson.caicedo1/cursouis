const express = require('express');
const app = express();
const miconexion = require('./conexion');

app.listen(1000, function(){
    console.log('Conectado al servidor.');
});
app.get('/',function(req,res){
    res.send('<h1>Servidor Ok</h1>');
});
//importar las rutas
const rutas = require('./rutas/routers');
app.use('/api',rutas);