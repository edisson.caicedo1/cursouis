const express = require('express');
const router = express.Router();
const controladorCliente = require('../controladores/controlador_cliente');

router.get('/listar',controladorCliente);
router.post('/agregar',controladorCliente);
module.exports = router;