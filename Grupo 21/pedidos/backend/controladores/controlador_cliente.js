const express = require('express');
const router = express.Router();
const modeloCliente = require('../modelos/modelo_cliente');

router.get('/listar',(req,res)=>{
    modeloCliente.find({}, (docs,err)=>{
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});
router.post('/agregar',(req, res)=>{
    const nuevoCliente = new modeloCliente({
        id : req.body.id,
        nombre : req.body.nombre,
        telefono : req.body.telefono,
        email : req.body.email
    });
    nuevoCliente.save(function(err){
        if(!err)
        {
            res.send('Registro almancenado correctamente');
        }
        else
        {
            res.send('No fue posible registrar el dato.');
        }
    })
});

module.exports=router;