//importar la dependencia mongoose
const mongoose = require('mongoose');
//Establecemos la ruta de conexión
mongoose.connect('mongodb://localhost:27017/Grupo21');
//A una constante asignarle la conexión
const miconexion = mongoose.connection;
//Validar la conexión si es correcta
miconexion.on('connected',()=>{console.log('Conectado a la base de datos');});
//Validar la conexión cuando no es correcta
miconexion.on('error',()=>{
    console.log('Error al conectarse a la base de datos, revisar...');
});
//Retornar la conexión
module.exports=mongoose;